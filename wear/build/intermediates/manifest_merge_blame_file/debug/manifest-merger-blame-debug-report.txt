1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.example.aficanwatch"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="26"
8-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml
10
11    <!-- Required to act as a custom watch face. -->
12    <uses-permission android:name="android.permission.WAKE_LOCK" />
12-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:6:5-68
12-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:6:22-65
13
14    <!-- Required for complications to receive complication data and open the provider chooser. -->
15    <uses-permission android:name="com.google.android.wearable.permission.RECEIVE_COMPLICATION_DATA" />
15-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:9:5-104
15-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:9:22-101
16
17    <uses-feature android:name="android.hardware.type.watch" />
17-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:11:5-64
17-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:11:19-61
18
19    <application
19-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:13:5-47:19
20        android:allowBackup="true"
20-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:14:9-35
21        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
21-->[androidx.core:core:1.1.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/f01b27bb220ecb79298884ef36a7a2fb/core-1.1.0/AndroidManifest.xml:24:18-86
22        android:debuggable="true"
23        android:icon="@mipmap/ic_launcher"
23-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:15:9-43
24        android:label="@string/app_name"
24-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:16:9-41
25        android:supportsRtl="true"
25-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:17:9-35
26        android:testOnly="true"
27        android:theme="@android:style/Theme.DeviceDefault" >
27-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:18:9-59
28        <meta-data
28-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:19:9-21:36
29            android:name="com.google.android.wearable.standalone"
29-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:20:13-66
30            android:value="true" />
30-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:21:13-33
31
32        <service
32-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:23:9-42:19
33            android:name="com.example.aficanwatch.MyWatchFace"
33-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:24:13-40
34            android:label="@string/my_analog_name"
34-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:25:13-51
35            android:permission="android.permission.BIND_WALLPAPER" >
35-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:26:13-67
36            <meta-data
36-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:27:13-29:54
37                android:name="android.service.wallpaper"
37-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:28:17-57
38                android:resource="@xml/watch_face" />
38-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:29:17-51
39            <meta-data
39-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:30:13-32:63
40                android:name="com.google.android.wearable.watchface.preview"
40-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:31:17-77
41                android:resource="@drawable/preview_analog" />
41-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:32:17-60
42            <meta-data
42-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:33:13-35:63
43                android:name="com.google.android.wearable.watchface.preview_circular"
43-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:34:17-86
44                android:resource="@drawable/preview_analog" />
44-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:35:17-60
45
46            <intent-filter>
46-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:37:13-41:29
47                <action android:name="android.service.wallpaper.WallpaperService" />
47-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:38:17-85
47-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:38:25-82
48
49                <category android:name="com.google.android.wearable.watchface.category.WATCH_FACE" />
49-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:40:17-102
49-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:40:27-99
50            </intent-filter>
51        </service>
52
53        <meta-data
53-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:44:9-46:69
54            android:name="com.google.android.gms.version"
54-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:45:13-58
55            android:value="@integer/google_play_services_version" />
55-->/home/coulibaly/AndroidStudioProjects/africanwatch/wear/src/main/AndroidManifest.xml:46:13-66
56
57        <uses-library
57-->[com.google.android.support:wearable:2.5.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/0b7e99c892f068e0697e709b7b1efde4/jetified-wearable-2.5.0/AndroidManifest.xml:10:9-12:40
58            android:name="com.google.android.wearable"
58-->[com.google.android.support:wearable:2.5.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/0b7e99c892f068e0697e709b7b1efde4/jetified-wearable-2.5.0/AndroidManifest.xml:11:13-55
59            android:required="false" />
59-->[com.google.android.support:wearable:2.5.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/0b7e99c892f068e0697e709b7b1efde4/jetified-wearable-2.5.0/AndroidManifest.xml:12:13-37
60
61        <activity
61-->[com.google.android.gms:play-services-base:17.0.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/bd8a4935a17dde33dc1d62d4f9914d3b/play-services-base-17.0.0/AndroidManifest.xml:23:9-26:75
62            android:name="com.google.android.gms.common.api.GoogleApiActivity"
62-->[com.google.android.gms:play-services-base:17.0.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/bd8a4935a17dde33dc1d62d4f9914d3b/play-services-base-17.0.0/AndroidManifest.xml:24:13-79
63            android:exported="false"
63-->[com.google.android.gms:play-services-base:17.0.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/bd8a4935a17dde33dc1d62d4f9914d3b/play-services-base-17.0.0/AndroidManifest.xml:25:13-37
64            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
64-->[com.google.android.gms:play-services-base:17.0.0] /home/coulibaly/.gradle/caches/transforms-2/files-2.1/bd8a4935a17dde33dc1d62d4f9914d3b/play-services-base-17.0.0/AndroidManifest.xml:26:13-72
65    </application>
66
67</manifest>
